$(function() {

	function trackEvent(eventName) {
		ga('send', 'event', eventName);
		yaCounter.reachGoal(eventName);
	}

	var $email = $('[name="email"]')
		, $csrf = $('meta[name="csrf-token"]')
		, $button = $('.submit-button').first()
	;

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $csrf.attr('content')
		}
	});

	$button.on('click', function(e) {
		e.preventDefault();
		trackEvent('btn_sendEmail');
		$.ajax({
			url: '/subscribe',
			data: {
				email: $email.val()
			},
			dataType: 'json',
			type: 'POST'
		}).done(function() {
			$('#email-help-text').text('').hide();
			$('.success-hide').hide();
			$('.success-show').show();
			trackEvent('email_send_success');
		}).fail(function(res) {
			$('#email-help-text').html(res.responseJSON.validationError.join('<br/>')).show();
			trackEvent('email_send_error');
		}).always(function() {
			$email.val('').css('text-align', 'center');
		});
	});

	$email.on('focus', function () {
		$(this).css('text-align', 'left');
		if ($(this).val().length == 0) {
			$(this).attr('placeholder', '');
		} else {

		}
	}).on('blur', function () {
		if ($(this).val().length == 0) {
			$(this).css('text-align', 'center');
		}
		$(this).attr('placeholder', $(this).data('placeholder'));
	});

	window.onYouTubePlayerAPIReady = function() {
		var player = new YT.Player('ytplayer', {
			height: '390',
			width: '100%',
			videoId: 'HLhGcvPj6z0',
			playerVars: { showinfo: 0 },
			events: {
				onReady: function(e) {
					e.target.mute();
					e.target.playVideo();

				},
				onStateChange: function(e) {
					switch (e.data) {
						case YT.PlayerState.ENDED:
							trackEvent('video_ended');
							break;
						case YT.PlayerState.PLAYING:
							trackEvent('video_playing');
							break;
						case YT.PlayerState.PAUSED:
							trackEvent('video_paused');
							break;
					}
				}
			}
		});
	}
});

