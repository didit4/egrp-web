$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".deleteObject").on('click', function (e) {
        e.preventDefault();
        $('.alert-danger').remove();
        var url = $(this).attr('href');

        if (confirm('Вы дейстительно хотите удалить?')) {
            $.ajax({
                type: 'DELETE',
                url: url, //resource
                success: function (response) {
                    if(response.error) {
                        $('.page-header').append('<div class="alert alert-danger" role="alert">' + response.message + '</div>');
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
    });

    if ($("#datepicker").length > 0) {
        $("#datepicker").datetimepicker({
            locale: 'ru'
        });
    }

    $(document).ready(function() {
        $('#myTabs').tab();
        $('#myTabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#myTabs a[href="' + hash + '"]').tab('show');


        $('.set-active').on('click', function(e) {
            console.log('123');
            $('form')
                .append('<input type="hidden" name="active" value="1" />')
                .submit();

        });

        var curr = $('.wrapper-list').data('curr');
        $('.add-rule1').on('click', function(){
            var clone = $('.sample').first().clone();
            curr += 1;
            clone.find('.list_text').attr('name', 'arrList1[' + curr + '][text]');
            clone.find('.list_val').attr('name', 'arrList1[' + curr + '][val]');
            $('.wrapper-list').append(clone);
        });

        var curr2 = $('.wrapper-list2').data('curr');
        $('.add-rule2').on('click', function(){
            var clone = $('.sample2').first().clone();
            curr2 += 1;
            clone.find('.list_text').attr('name', 'arrList2[' + curr2 + '][text]');
            $('.wrapper-list2').append(clone);
        });

        var curr3 = $('.wrapper-list3').data('curr');
        $('.add-rule3').on('click', function(){
            var clone = $('.sample3').first().clone();
            curr3 += 1;
            clone.removeClass('hidden');
            clone.find('.list_header').attr('name', 'arrList3[' + curr3 + '][header]');
            clone.find('.list_text').attr('name', 'arrList3[' + curr3 + '][text]');
            $('.wrapper-list3').append(clone);
        });

        $('.add-rule4').on('click', function(){
            var clone = $('.sample4').first().clone();
            var currItem = $(this).data('curritem');
            var curr4 = $('.wrapper-lists' + currItem + ' .sample4').length;
            console.log('currItem', currItem);
            console.log('curr4', curr4);
            curr4 += 1;
            clone.removeClass('hidden');
            clone.find('.list_list').attr('name', 'arrList3[' + currItem + '][list][' + curr4 + ']');
            $('.wrapper-lists' + currItem).append(clone);
        });


        $('.remove-item').on('click', function(){
            $(this).closest('.form-group').remove();
        });

        $('#sortable').sortable({
            handle: '.sort-handler',
            items: ' > tr',
            stop: function () {
                var sortedItems = [];
                $('#sortable > tr').each(function () {
                    sortedItems.push($(this).data('team_id'));
                });

                $.ajax({
                    type: "POST",
                    url: '/admin/teams/reorder',
                    data: {items: sortedItems},
                    dataType: 'JSON'
                });
            }
        });

        $('.emulate-req').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/reqs',
                data: {isEmulate: true},
                dataType: 'JSON',
                success: function (response) {
                    if(!response.success) {
                        $('.page-header').append('<div class="alert alert-danger" role="alert">' + response.errors + '</div>');
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $('.send-price').on('click', function(){
            var price = $('input[name=value]').val();
            $.ajax({
                type: "PUT",
                url: '/admin/settings/1',
                data: {value: price},
                success: function (response) {
                    if(!response.success) {
                        $('.page-header').append('<div class="alert alert-danger" role="alert">' + response.errors + '</div>');
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $('.download').on('click', function(e){
            e.preventDefault();
            var month = $('#sel-months').val();
            var url = $(this).attr('href');
            var param = '';
            if (!$('.all_stat').is(':checked')) {
                param = '?month=' + month;
            }
            window.location = url + param;
        });

        $('#sel-months').on('change', function(){
            var month = $(this).val();
            $.ajax({
                type: "GET",
                url: '/admin/reqs/change_month',
                data: {month: month},
                success: function (response) {
                    $('.m-sum').html(response.sumCurrMonth);
                }
            });
        });


        var typingTimer;
        var doneTypingInterval = 1000;
        var $input = $('.search-input');

        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(showPreloader, doneTypingInterval);
        });

        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        function doneTyping () {
            var data = {
                num: $.trim($('.search-input[name=num]').val()),
                loc: $.trim($('.search-input[name=loc]').val()),
                name: $.trim($('.search-input[name=name]').val()),
                phone: $.trim($('.search-input[name=phone]').val())
            };
            window.location.search = "?" + $.param(data);
        }

        function showPreloader () {
            $('.preloader').show();
            setTimeout(doneTyping, doneTypingInterval);
        }

        $('.can-switch').on('click', function(){
            var canSwitch = true;
            if(!$(this).data('modal')) {
                if ($(this).hasClass('confirm-alert')) {
                    canSwitch = confirm("Будет отправлено письмо пользователю! Вы уверены?");
                }
                if (canSwitch) {
                    window.location = $(this).data('status');
                }
            }
        });

        $('.send-answer').on('click', function(e){
            e.preventDefault();
            var canSwitch = confirm("Будет отправлено письмо пользователю! Вы уверены?");
            if (canSwitch) {
                $('form').submit();
            }
        });

        $("input:file").change(function (){
            $('.load-file').show();
        });

        $('.load-file').on('click', function(){
            $('.preloaderFile').show();
            $(this).hide();
        });

        $('select[name=region_id]').on('change', function(e){
            e.preventDefault();
            $('input[name=region_name]').val($(this).find('option:selected').text());
            $.ajax({
                type: 'GET',
                url: '/api/v1/req/get_areas',
                data: {
                    region_id: $(this).val()
                },
                success: function (response) {
                    $('select[name=area_id]').find('option').remove();
                    $('select[name=locality_id]').find('option').remove();
                    if (response.areas.length) {
                        $('select[name=area_id]').append($('<option>', {
                            text: 'Выберите район'
                        }));
                        $.each(response.areas, function (i, item) {
                            $('select[name=area_id]').append($('<option>', {
                                value: item.id,
                                text: item.name
                            }));
                        });
                    }
                }
            });
        });

        $('select[name=area_id]').on('change', function(e){
            e.preventDefault();
            $('input[name=area_name]').val($(this).find('option:selected').text());
            $.ajax({
                type: 'GET',
                url: '/api/v1/req/get_places',
                data: {
                    area_id: $(this).val()
                },
                success: function (response) {
                    $('select[name=locality_id]').find('option').remove();
                    if (response.places.length) {
                        $('select[name=locality_id]').append($('<option>', {
                            text: 'Выберите населенный пункт '
                        }));
                        $.each(response.places, function (i, item) {
                            $('select[name=locality_id]').append($('<option>', {
                                value: item.id,
                                text: item.name
                            }));
                        });
                    }
                }
            });
        });

        $('select[name=locality_id]').on('change', function(e){
            e.preventDefault();
            $('input[name=locality_name]').val($(this).find('option:selected').text());
        });

        $('select[name=street_type]').on('change', function(e){
            e.preventDefault();
            $('input[name=street_type_name]').val($(this).find('option:selected').text());
        });
    });

});
$(function () {

    $('.ckEditor').each(function () {
        CKEDITOR.replace(this, {
            language: 'ru',
            extraPlugins: 'quote',
            allowedContent: true,
            toolbar: [
                {
                    name: 'document',
                    items: ['Source', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat', '-', 'Styles', 'Format', 'Font', 'FontSize']
                },
                '/',
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', 'TextColor', 'BGColor', 'Maximize', 'ShowBlocks'  ]
                },
                {name: 'links', items: ['Link', 'Unlink']},
                {
                    name: 'insert',
                    items: ['Image', 'Table', 'SpecialChar', 'PageBreak']
                }

            ]
        });
    })
});
