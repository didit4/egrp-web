CKEDITOR.dialog.add('quoteDialog', function (editor) {
	return {
		title: 'Создание цитаты',
		minWidth: 400,
		minHeight: 200,
		contents: [
			{
				id: 'tab-basic',
				label: '',
				elements: [
					{
						type: 'textarea',
						id: 'quote',
						label: 'Цитата',
						validate: CKEDITOR.dialog.validate.notEmpty("Цитата не должна быть пустой")
					}
				]
			}
		],
		onOk: function () {
			var dialog = this
				, quote = editor.document.createElement('div')
				, quoteVal = dialog.getValueOf('tab-basic', 'quote')
				;

			quote.setAttribute('class', 'article-block-cav');
			quote.setText(quoteVal);

			editor.insertElement(quote);
		}
	};
});