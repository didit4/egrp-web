@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.bars.offers.update', $bar, $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.bars.offers.index', $bar], 'class' => 'form-horizontal']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('text', 'Описание оффера:', ['class' => 'control-label']) !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.bars.edit', 'Отмена', [$bar, '#offers'], array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}

