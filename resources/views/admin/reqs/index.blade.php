@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Список заявок @parent @stop

{{-- Content --}}
@section('content')
    {!! Breadcrumbs::render('reqs') !!}
    <div class="page-header without-top">
        <div class="row req_price">
            <div class="col-sm-8"></div>
            <div class="col-sm-4">
                <div class="row">
                    @if(!\Auth::getUser()->isAdmin())
                        <div class="col-sm-6">
                        </div>
                    @endif
                    <div class="col-sm-6 req-pr-li">
                        Цена заявки: {{$price->value}} руб.
                    </div>
                    @if(\Auth::getUser()->isAdmin())
                        <div class="col-sm-6">
                            <div class="btn btn-default"  data-toggle="modal" data-target="#myModal">Изменить цену заявки</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <h3>Список заявок</h3>
                <em class="bg-warning">Здесь отображены заявки поступившие из мобильного приложения.</em>
            </div>
            <div class="col-sm-4">
                <div class="new-req-wrapper">
                    <div class="row">
                        @if(!\Auth::getUser()->isAdmin())
                            <div class="col-sm-6">
                            </div>
                        @endif
                        <div class="col-sm-6">
                            Сегодня новых: {{$countTodayRequest}}<br>
                            Сумма: {{$sumTodayRequest}} руб.
                        </div>
                        @if(\Auth::getUser()->isAdmin())
                            <div class="col-sm-6">
                                <a class="btn btn-default" href="{{ route('admin.reqs.statistic') }}">Статистика по заявкам</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table id="table" class="table table-striped table-hover table-verical">
        <thead>
        <tr>
            <th>#</th>
            <th>Статус</th>
            <th>Номер</th>
            <th>Нас. пункт</th>
            <th>Конт. лицо</th>
            <th>Телефон</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" name="num" class="form-control search-input" placeholder="Поиск" value="{{isset($params['num']) ? $params['num'] : ''}}">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" name="loc" class="form-control search-input" placeholder="Поиск" value="{{isset($params['loc']) ? $params['loc'] : ''}}">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control search-input" placeholder="Поиск" value="{{isset($params['name']) ? $params['name'] : ''}}">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control search-input" placeholder="Поиск" value="{{isset($params['phone']) ? $params['phone'] : ''}}">
                    </div>
                </td>
            </tr>
        @foreach ($reqs as $req)
            <tr class="data-row {{ $req->isBusy() ? 'danger' : '' }}">
                <td>{{ $req->id }}</td>
                <td class="td-centered">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="label {{ $req->getStatusClassColor() }} full-width">{{ $req->getStatusName() }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {{ $req->updated_at }}
                        </div>
                    </div>
                </td>
                <td>
                    @if( $req->isBusy())
                        С заявкой в данный момент работает другой оператор
                    @else
                        <a href="{{ route('admin.reqs.show', $req->id) }}">{{ $req->getNum() ? : 'Просмотр' }}</a>
                    @endif
                </td>
                <td>
                    {{ $req->locality_name }}
                </td>
                <td>
                    {{ $req->name }}
                </td>
                <td>
                    {{ $req->phone }}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    @if($message)
        <div class="empty-result">
            <h2>{{$message}}</h2>
        </div>
    @endif
    <?php echo $reqs->appends($params)->render(); ?>
            <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Изменение цены заявки. Сейчас {{$price->value}} руб.</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="value" class="form-control" placeholder="Введите новую цену для одной заявки">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary send-price">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <div class="preloader">
        <img src="/img/preloader.svg" />
    </div>
@stop
