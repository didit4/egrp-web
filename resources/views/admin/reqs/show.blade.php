@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Список заявок @parent @stop

{{-- Content --}}
@section('content')
    {!! Breadcrumbs::render('reqs_show', $model) !!}
    <div class="page-header without-top">
        <div class="row">
            <div class="col-sm-7">
                <a class="btn btn-default" href="{{ route('admin.reqs.index') }}"><- Вернуться в общий список заявок</a>
                <h3>Заявка # {{ $model->id }}</h3>
                <em class="bg-warning">Здесь отображена конкретная заявка. Нажмите на новый статус заявки, чтобы изменить его.</em>
                @if($message)<br/>
                    <em class="bg-danger">{{ $message }}</em>
                @endif
            </div>
            <div class="col-sm-5">
                <div class="new-req-wrapper">
                    <div class="row" style="text-align: right;">
                        <a class="btn btn-default" href="{{ route('admin.reqs.edit', $model->id) }}">Редактировать заявку</a>
                        <a href="{{ route('admin.reqs.first', [$model->id]) }}" class="btn btn-default">Первоначальная заявка</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-4">
            <b>{{$model->name}}&nbsp;{{$model->phone}}</b>
        </div>
        <div class="col-sm-8">
            {{$model->email}}
        </div>
    </div>
    <div class="row show-grid show-grid_gray statuses-wrapper">
        <div class="col-sm-2">
            <b>Cтатус</b>
        </div>
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 0 ? 'label-danger' : 'label-light_gray'}} full-width" data-status="{{ route('admin.reqs.switch_status', [$model->id, 0]) }}">{{isset($statuses[0]) ? $statuses[0] : 'Новая'}}</span>
            {{$model->created_at}}<br>
        </div>
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 1 ? 'label-warning' : ($model->status == 0 ? 'label-gray can-switch' : 'label-light_gray')}} full-width" data-status="{{ route('admin.reqs.switch_status', [$model->id, 1]) }}">{{isset($statuses[1]) ? $statuses[1] : 'Обрабатывается'}}</span>
            @if($model->status > 0)
                {{$model->processed_at}}<br>
                {{$model->user ? $model->user->name : ''}}<br>
            @endif
        </div>
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 2 ? 'label-primary' : ($model->status == 1 ? 'label-gray can-switch' : 'label-light_gray')}} full-width" data-status="{{ route('admin.reqs.switch_status', [$model->id, 2]) }}">{{isset($statuses[2]) ? $statuses[2] : 'В Росреестре'}}</span>
            @if($model->status > 1)
                {{$model->ros_at}}<br>
                {{$model->user ? $model->user->name : ''}}<br>
            @endif
        </div>
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 3 ? 'label-info' : ($model->status == 2 && $model->file ? 'label-gray can-switch' : 'label-light_gray')}} full-width" data-modal="1" data-toggle="modal" data-target="#myModal">{{isset($statuses[3]) ? $statuses[3] : 'Получен XML'}}</span>
            @if($model->status > 2)
                {{$model->xml_at}}<br>
                {{$model->user ? $model->user->name : ''}}<br>
            @endif
            @if($model->status > 1 and $model->file)
                <a href="{{$model->getFilePath()}}" target="_blank">Посмотреть xml</a>
            @endif
            @if($model->status > 1 and $model->file2)
                <a href="{{$model->getFile2Path()}}" target="_blank">Посмотреть pdf/zip</a>
            @endif
        </div>
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 4 ? 'label-success' : ($model->status == 3 ? 'label-gray can-switch confirm-alert' : 'label-light_gray')}} full-width" data-status="{{ route('admin.reqs.switch_status', [$model->id, 4]) }}">{{isset($statuses[4]) ? $statuses[4] : 'Отправлено на email'}}</span>
            @if($model->status > 3)
                {{$model->sent_at}}<br>
                {{$model->user ? $model->user->name : ''}}<br>
            @endif
        </div>
    </div>
    <div class="row show-grid show-grid_gray statuses-wrapper">
        <div class="col-sm-2 td-centered status-item">
            <span class="label {{$model->status == 5 ? 'label-danger' : 'label-gray can-switch confirm-alert'}} full-width" data-status="{{ route('admin.reqs.switch_status', [$model->id, 5]) }}">{{isset($statuses[5]) ? $statuses[5] : 'Нет в Росреестре'}}</span>
            @if($model->status == 5)
                {{$model->not_at}}<br>
                {{$model->user ? $model->user->name : ''}}<br>
            @endif
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
            <b>{{$model->getNumName()}}</b>
        </div>
        <div class="col-sm-2">
            {{$model->getNum()}}
        </div>
        @if($model->cadastral_num && $model->region_name)
            <div class="col-sm-2">
                {{$region === null ? 'Не удалось определить регион' : $region->getFullName()}}
            </div>
        @endif
    </div>
    @if($model->type)
        <hr/>
        <div class="row show-grid">
            <div class="col-sm-2">
                <b>Тип заявки</b>
            </div>
            <div class="col-sm-2">
                {{$model->reqType->name}}
            </div>
        </div>
    @endif
    <hr/>
    <div class="row show-grid">
        <div class="col-sm-2">
            <b>Адрес</b>
        </div>
        <div class="col-sm-3">
            <b>Регион</b> <br>
            @if($model->cadastral_num && !$model->region_name)
                {{$region === null ? 'Не удалось определить регион' : $region->getFullName()}}
            @else
                {{$model->region_name}}
            @endif
        </div>
        <div class="col-sm-3">
            <b>Район</b> <br>
            {{$model->area_name}}
        </div>
        <div class="col-sm-3">
            <b>Населенный пункт</b> <br>
            {{$model->locality_name}}
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
            <b>Тип улицы</b> <br>
            {{$model->street_type_name}}
        </div>
        <div class="col-sm-3">
            <b>Улица</b> <br>
            {{$model->street_name}}
        </div>
        <div class="col-sm-3">
            <b>Дом</b> <br>
            {{$model->house}}
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
            <b>Квартира</b> <br>
            {{$model->room}}
        </div>
        <div class="col-sm-3">
            <b>Строение</b> <br>
            {{$model->building}}
        </div>
        <div class="col-sm-3">
            <b>Корпус</b> <br>
            {{$model->housing}}
        </div>
    </div>
    @if ($model->address)
        <div class="row show-grid">
            <div class="col-sm-2">
                <b>Рукописный адрес</b><br>
                {{$model->address}}
            </div>
        </div>
    @endif
    <hr/>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('admin.reqs.load_file', $model->id) }}" method="POST" enctype="multipart/form-data">
                        <label for="file">Загрузка XML от Росреестра</label>
                        <input type="file" name="file" id="file" accept=".xml"><br>
                        <label for="file2">Загрузка итогового PDF|ZIP</label>
                        <input type="file" name="file2" id="file2" accept=".pdf,.zip"><br>
                        <button type="submit" class="btn btn-default load-file">{{ $model->status == 4 ? 'Обновить файлы и повторно отправить их на email':'Загрузить файл' }}</button><img class="preloaderFile" src="/img/pre.svg">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        var interval = setInterval(function() {
            $.ajax({
                type: "GET",
                url: '/admin/reqs/' + {{ $model->id }} + '/update_watch'
            });
        }, 30000)
    </script>
@stop
