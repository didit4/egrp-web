@extends('admin.layouts.default')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Редактировать заявку # {{ $model->id }}</h1>
            <em class="bg-warning">Здесь можно редактировать конкретную заявку. Статус здесь не изменить</em>
            {!! Breadcrumbs::render('reqs_edit', $model) !!}
        </div>
        <div class="col-md-12">
            @include('admin.reqs._form')
        </div>

    </div>
    <script>
        var interval = setInterval(function() {
            $.ajax({
                type: "GET",
                url: '/admin/reqs/' + {{ $model->id }} + '/update_watch'
            });
        }, 30000)
    </script>

@stop
