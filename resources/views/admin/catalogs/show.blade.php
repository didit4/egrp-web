@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Бары @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>{{ $model->name }}</h3>
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.catalogs.items.create', 'Добавить', [$model->id], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>
    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Действия</th>
            <th>Название</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($model->items as $item)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.catalogs.items.edit', [$model->id, $item->id]) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.catalogs.items.destroy', [$model->id, $item->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>
                    @if($item->img)
                        <img height="20" src="{{ $item->getImg() }}" />
                    @endif
                    {{ $item->name }}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
