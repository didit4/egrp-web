@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Бары @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Справочники</h3>
        @if(!$catalogs->count())
            <em class="bg-warning">Ни одного справочника еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.catalogs.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Название</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($catalogs as $catalog)
            <tr>
                <td>
                    <a href="{!!  URL::route('admin.catalogs.show', [$catalog->id]) !!}">
                        {{ $catalog->name }}
                    </a>
                </td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.catalogs.edit', [$catalog->id]) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.catalogs.destroy', [$catalog->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
