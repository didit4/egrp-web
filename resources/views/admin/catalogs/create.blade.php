@extends('admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Добавить справочник</h1>
            <hr>
        </div>

        <div class="col-md-8">
            @include('admin.catalogs._form')
        </div>

    </div>
@stop
