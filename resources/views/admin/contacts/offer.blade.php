@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Оферта @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Оферта</h3>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($contact)
                {!! HTML::linkRoute('admin.contacts.offer_edit', 'Редактировать', [$contact->id], ['class' => 'btn btn-md btn-default']) !!}
            @else
                {!! HTML::linkRoute('admin.contacts.create', 'Редактировать', [], ['class' => 'btn btn-md btn-default']) !!}
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($contact)
                {!! $contact->text !!}
            @endif
        </div>
    </div>

@stop
