@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Контакты @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Контакты</h3>
        <em class="bg-warning">Здесь отображена информация о контактах</em>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($contact)
                {!! HTML::linkRoute('admin.contacts.edit', 'Редактировать', [$contact->id], ['class' => 'btn btn-md btn-default']) !!}
            @else
                {!! HTML::linkRoute('admin.contacts.create', 'Редактировать', [], ['class' => 'btn btn-md btn-default']) !!}
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($contact)
                {{$contact->email}}<br/>
                {!! $contact->text !!}
            @endif
        </div>
    </div>

@stop
