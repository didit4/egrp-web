@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Вопросы от пользователей @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Вопросы от пользователей</h3>
        @if(!$questions->count())
            <em class="bg-warning">Ни одного вопроса еще не задано.</em>
        @else
            <em class="bg-warning">Здесь отображены вопросы от пользователей.</em>
        @endif
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="w10">Действия</th>
            <th>Имя</th>
            <th>Email</th>
            <th class="w30">Вопрос</th>
            <th class="w30">Ответ</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($questions as $question)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                            <a href="{!!  URL::route('admin.questions.edit', [$question->id]) !!}" class="btn btn-default"
                               title="Ответить">
                                @if (!$question->is_send)
                                    Ответить
                                @else
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                @endif
                            </a>

                        <a href="{!!  URL::route('admin.questions.destroy', [$question->id]) !!}"
                           class="btn btn-default deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>{{ $question->name }}</td>
                <td>{{ $question->email }}</td>
                <td><?echo str_limit($question->text, 150); ?></td>
                <td><?echo str_limit($question->answer, 150); ?></td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <?php echo $questions->render(); ?>
@stop
