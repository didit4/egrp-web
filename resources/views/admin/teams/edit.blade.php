@extends('admin.layouts.default')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Редактировать команду</h1>
            <hr>
        </div>

        <div class="col-md-8">
            @include('admin.teams._form')
        </div>

    </div>

@stop
