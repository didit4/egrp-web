@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.reqtypes.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.reqtypes.index'], 'class' => 'form-horizontal']) !!}
@endif


<div class="form-group">
    {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! HTML::linkRoute('admin.reqtypes.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
    {!! Form::submit(isset($model) ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-default']) !!}
</div>

{!! Form::close() !!}

