<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>


    <link rel="icon" href="/static/img/favicon.ico">

    <title>@yield('title')</title>

    {!! HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}
    {!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') !!}
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.3/jquery-ui.js') !!}
    {!! HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') !!}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.0/fullcalendar.min.css">
    {!! HTML::style('/libs/datetime-picker/css/bootstrap-datetimepicker.min.' . $staticversion . '.css') !!}
    <link type="text/css" rel="stylesheet" href="/admin.{{$staticversion}}.css" />
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript" />
@section('styles')
    @show

            <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    @include('admin.layouts.blocks.navbar')
</nav>

<div class="container" id="main-container">
    @yield('content')
</div>
{!! HTML::script('/ckeditor/ckeditor.js') !!}
{!! HTML::script('/admin.' . $staticversion . '.js') !!}
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


{{-- Scripts --}}
@section('scripts')
    {{--{!! HTML::script('js/libs/ckeditor/ckeditor.js') !!}--}}
    {{--{!! HTML::script('js/jquery-ui.min.js') !!}--}}
    {{--{!! HTML::script('/js/jquery.beforeafter.js') !!}--}}
    {!! HTML::script('/libs/moment.min.' . $staticversion . '.js') !!}
    {!! HTML::script('/libs/locale/ru.' . $staticversion . '.js') !!}
    {!! HTML::script('/libs/datetime-picker/js/bootstrap-datetimepicker.min.' . $staticversion . '.js') !!}
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.7.0/fullcalendar.min.js') !!}

@show

<footer class="footer">
    <div class="container">
        {{--<p class="text-muted">Админка футер.</p>--}}
    </div>
</footer>
</body>
</html>
