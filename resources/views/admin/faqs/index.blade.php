@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Вопросы и ответы (FAQ) @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Вопросы и ответы (FAQ)</h3>
        @if(!$faqs->count())
            <em class="bg-warning">Ни одного вопроса еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
        @else
            <em class="bg-warning">Здесь отображены вопросы и ответы на самые популярные темы.</em>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.faqs.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Действия</th>
            <th>Вопрос</th>
            <th>Ответ</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($faqs as $faq)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.faqs.edit', [$faq->id]) !!}" class="btn btn-default"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.faqs.destroy', [$faq->id]) !!}"
                           class="btn btn-default deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.faqs.switchActive', [$faq->id]) !!}"
                           class="btn {!! $faq->active ? 'btn-primary' : 'btn-default' !!}" title="{!! $faq->active ? 'В черновики' : 'Опубликовать' !!}">
                            <span class="glyphicon {!! $faq->active ? 'glyphicon-eye-open' : 'glyphicon-eye-close' !!}" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>{{ $faq->question }}</td>
                <td>{{ $faq->answer }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <?php echo $faqs->render(); ?>
@stop
