@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.faqs.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.faqs.index'], 'class' => 'form-horizontal']) !!}
@endif


<div class="form-group">
    {!! Form::label('question', 'Вопрос:', ['class' => 'control-label']) !!}
    {!! Form::text('question', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer', 'Ответ:', ['class' => 'control-label']) !!}
    {!! Form::textarea('answer', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! HTML::linkRoute('admin.faqs.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
    {!! Form::submit(isset($model) ? 'Обновить' : 'Сохранить как черновик', ['class' => 'btn btn-default']) !!}
    @if (!isset($model))
        <div class="btn btn-default set-active">Сохранить и опубликовать</div>
    @endif
</div>

{!! Form::close() !!}

