@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Правила @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Правила</h3>
        <em class="bg-warning">Информация о баллах.</em>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($rule)
                {!! HTML::linkRoute('admin.rules.edit', 'Редактировать', [$rule->id], ['class' => 'btn btn-md btn-success']) !!}
            @else
                {!! HTML::linkRoute('admin.rules.create', 'Редактировать', [], ['class' => 'btn btn-md btn-success']) !!}
            @endif
        </div>
    </div>
    @if ($rule)
        <div class="row">
            <div class="col-xs-8">
                {!! $rule->text !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <h4>{{$rule->title1}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                @foreach($rule->getList1() as $listItem)
                    <div class="row">
                        @if (isset($listItem['text']))
                            <div class="col-xs-10">
                                {{$listItem['text']}}
                            </div>
                        @endif
                        @if (isset($listItem['val']))
                            <div class="col-xs-2">
                                {{$listItem['val']}}
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <h4>{{$rule->title2}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                @foreach($rule->getList2() as $listItem)
                    @if (isset($listItem['text']))
                        <div class="row">
                            <div class="col-xs-12">
                                {{$listItem['text']}}
                            </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endif
@stop
