@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.rules.update', $model->id], 'class' => '']) !!}
@else
    {!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.rules.index'], 'class' => '']) !!}
@endif
<div class="panel panel-default">
    <div class="panel-body">
        <div class="wrapper-list3" data-curr="{{count($model->geMainText())}}">
            <div class="form-group">
                {!! Form::label('text', 'Основонй текст:', ['class' => 'control-label']) !!}
                <div class="btn btn-default add-rule3" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
            </div>
            <div class="form-group sample4 hidden">
                <div class="row">
                    <div class="col-xs-1">
                        <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                    </div>
                    <div class="col-xs-11">
                        {!! Form::text('hidden', null, ['class' => 'form-control list_list']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group sample3 hidden">
                {!! Form::label('text', 'Заголовок:', ['class' => 'control-label']) !!}
                <div class="row">
                    <div class="col-xs-1">
                        <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                    </div>
                    <div class="col-xs-11">
                        {!! Form::text('hidden', null, ['class' => 'form-control list_header']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        {!! Form::label('text', 'Текст:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('hidden', null, ['class' => 'form-control list_text', 'rows' => '4']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        {!! Form::label('text', 'Список:', ['class' => 'control-label']) !!}
                        <div class="btn btn-default add-rule4" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
                    </div>
                    <div class="wrapper-lists"></div>
                </div>
            </div>
            @if ($model->geMainText())
                @foreach($model->geMainText() as $key => $listItem)
                    <div class="form-group sample3">
                        {!! Form::label('text', 'Заголовок:', ['class' => 'control-label']) !!}
                        <div class="row">
                            <div class="col-xs-1">
                                <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                            </div>
                            <div class="col-xs-11">
                                {!! Form::text('arrList3[' . $key . '][header]',  isset($listItem['header']) ? $listItem['header'] : null, ['class' => 'form-control list_header']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('text', 'Текст:', ['class' => 'control-label']) !!}
                                {!! Form::textarea('arrList3[' . $key . '][text]', isset($listItem['text']) ? $listItem['text'] : null, ['class' => 'form-control list_text', 'rows' => '4']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('text', 'Список:', ['class' => 'control-label']) !!}
                                    <div class="btn btn-default add-rule4" data-currItem="{{ $key }}" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="wrapper-lists wrapper-lists{{ $key }}" data-curr="{{ isset($listItem['list']) ? count($listItem['list']) - 1 : 0}}">
                                    @if (isset($listItem['list']) && $listItem['list'])
                                        @foreach($listItem['list'] as $_key => $listListItem)
                                            <div class="form-group sample4">
                                                <div class="row">
                                                    <div class="col-xs-1">
                                                        <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                                                    </div>
                                                    <div class="col-xs-11">
                                                        {!! Form::text('arrList3[' . $key . '][list][' . $_key . ']', $listListItem, ['class' => 'form-control list_list']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('title1', 'Заголовок о баллах:', ['class' => 'control-label']) !!}
    {!! Form::text('title1', null, ['class' => 'form-control']) !!}
</div>
@if (isset($model))
    <div class="wrapper-list" data-curr="{{count($model->getList1()) - 1}}">
        <div class="form-group">
            {!! Form::label('list1', 'Список баллов:', ['class' => 'control-label']) !!}
            <div class="btn btn-default add-rule1" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
        </div>
        @foreach($model->getList1() as $key => $listItem)
            <div class="form-group sample">
                <div class="row">
                    <div class="col-xs-1">
                        <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                    </div>
                    <div class="col-xs-9">
                        {!! Form::text('arrList1[' . $key . '][text]', isset($listItem['text']) ? $listItem['text'] : null, ['class' => 'form-control list_text']) !!}
                    </div>
                    <div class="col-xs-2">
                        {!! Form::text('arrList1[' . $key . '][val]', isset($listItem['val']) ? $listItem['val'] : null, ['class' => 'form-control list_val']) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="wrapper-list" data-curr="0">
        <div class="form-group">
            {!! Form::label('list1', 'Список баллов:', ['class' => 'control-label']) !!}
            <div class="btn btn-default add-rule1" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
        </div>
        <div class="form-group sample">
            <div class="row">
                <div class="col-xs-1">
                    <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="col-xs-9">
                    {!! Form::text('arrList1[0][text]', null, ['class' => 'form-control list_text']) !!}
                </div>
                <div class="col-xs-2">
                    {!! Form::text('arrList1[0][val]', null, ['class' => 'form-control list_val']) !!}
                </div>
            </div>
        </div>
    </div>
@endif
<div class="form-group">
    {!! Form::label('title2', 'Заголовок о планах:', ['class' => 'control-label']) !!}
    {!! Form::text('title2', null, ['class' => 'form-control']) !!}
</div>
@if (isset($model))
    <div class="wrapper-list2" data-curr="{{count($model->getList2()) - 1}}">
        <div class="form-group">
            {!! Form::label('list2', 'Список планов:', ['class' => 'control-label']) !!}
            <div class="btn btn-default add-rule2" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
        </div>
        @foreach($model->getList2() as $key => $listItem)
            <div class="form-group sample2">
                <div class="row">
                    <div class="col-xs-1">
                        <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                    </div>
                    <div class="col-xs-11">
                        {!! Form::text('arrList2[' . $key .'][text]', isset($listItem['text']) ? $listItem['text'] : null, ['class' => 'form-control list_text']) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="wrapper-list2" data-curr="0">
        <div class="form-group">
            {!! Form::label('list2', 'Список планов:', ['class' => 'control-label']) !!}
            <div class="btn btn-default add-rule2" title="добавить пункт"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
        </div>
        <div class="form-group sample2">
            <div class="row">
                <div class="col-xs-1">
                    <div class="btn btn-default remove-item" title="удалить пункт"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="col-xs-11">
                    {!! Form::text('arrList2[0][text]', null, ['class' => 'form-control list_text']) !!}
                </div>
            </div>
        </div>
    </div>
@endif
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.catalogs.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}

