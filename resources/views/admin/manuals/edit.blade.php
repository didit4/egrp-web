@extends('admin.layouts.default')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Редактировать мануал</h1>
            <hr>
        </div>

        <div class="col-md-8">
            @include('admin.manuals._form')
        </div>

    </div>

@stop
