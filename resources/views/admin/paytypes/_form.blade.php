@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.paytypes.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.paytypes.index'], 'class' => 'form-horizontal']) !!}
@endif


<div class="form-group">
    {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('percent', 'Комиссия(%):', ['class' => 'control-label']) !!}
    {!! Form::text('percent', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('type', 'Тип(нужен для отправки из приложения):', ['class' => 'control-label']) !!}
    {!! Form::text('type', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! HTML::linkRoute('admin.paytypes.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
    {!! Form::submit(isset($model) ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-default']) !!}
</div>

{!! Form::close() !!}

