@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.users.update',$model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.users.index'], 'class' => 'form-horizontal']) !!}
@endif

{!! Form::hidden('id', null,['class'=>'form-control']) !!}

<div class="form-group">
    {!! Form::label('name', 'Имя:', ['class' => 'control-label']) !!}
    {!! Form::text('name',null ,['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('role_id', 'Роль:', ['class' => 'control-label']) !!}
    {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    @if (!isset($model))
        {!! Form::label('password', 'Пароль:') !!}
        {!! Form::text('password', null, ['class'=>'form-control']) !!}
    @else
        {!! Form::label('password', 'Новый пароль:') !!}
        <input type="password" name="password" class="form-control" />
    @endif
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.users.index', 'Отмена', null, array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}
