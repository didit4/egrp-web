@extends('admin.layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <p>
                <strong>Имя:</strong>
                {{ $model->name }}
            </p>

            @if($model->vk_id)
                <p>
                    <strong>Vk ID:</strong>
                    {{ $model->vk_id }}
                </p>
            @endif

            @if($model->fb_id)
                <p>
                    <strong>Fb ID:</strong>
                    {{ $model->fb_id }}
                </p>
            @endif
            <p>
                <strong>Очки:</strong>
                {{ $model->points }}
            </p>
            @if($model->points)
                <table id="table" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Описание</th>
                        <th>Очки</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($points as $point)
                        <tr>
                            <td>{{ $point->descr }}</td>
                            <td>{{ $point->val }}</td>
                            <td>{{ $point->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            {!!  HTML::linkRoute('admin.users.index', 'Назад', null, array('class' => 'btn btn-sm btn-primary')) !!}

        </div>
    </div>
@stop
