@extends('admin.layouts.default')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Редактировать бар</h1>
            {!! Breadcrumbs::render('bars_edit', $model) !!}
        </div>

        <div class="col-md-8">
            @include('admin.bars._form')
        </div>

    </div>

@stop
