@extends('admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Добавить бар</h1>
        </div>

        <div class="col-md-8">
            @include('admin.bars._form')
        </div>

    </div>
@stop
