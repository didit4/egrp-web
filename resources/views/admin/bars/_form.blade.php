@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
@if (isset($model))
{!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.bars.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
{!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.bars.index'], 'class' => 'form-horizontal']) !!}
@endif
<!-- Nav tabs -->
<ul id="myTabs" class="nav nav-tabs" role="tablist">
    @if (isset($model))
        <li class="active"><a href="#calendar" role="tab" data-toggle="tab">Календарь</a></li>
    @endif
    <li @if (!isset($model)) class="active" @endif><a href="#edit" role="tab" data-toggle="tab">Редактирование</a></li>
    @if (isset($model))
        <li><a href="#sportEvents" role="tab" data-toggle="tab">Игры</a></li>
        <li><a href="#offers" role="tab" data-toggle="tab">Офферы</a></li>
        <li><a href="#info" role="tab" data-toggle="tab">Инфо</a></li>
    @endif
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane @if (isset($model))active @endif" id="calendar">
        <div id='calendar'></div>
    </div>
    <div role="tabpanel" class="tab-pane @if (!isset($model))active @endif" id="edit">
        <div class="form-group">
            {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('phone', 'Телефон:', ['class' => 'control-label']) !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('schedule', 'График работы:', ['class' => 'control-label']) !!}
            @for($i = 0; $i < 7; $i++)
                <div class="row">
                    <div class="col-xs-1">{{ $weeksDays[$i]  }}</div>
                    <div class="col-xs-4">
                        @if(isset($schedule) && isset($schedule[$i]))
                            {!! Form::text('schedule[' . $i .']', $schedule[$i], ['class' => 'form-control']) !!}
                        @else
                            {!! Form::text('schedule[' . $i .']', null, ['class' => 'form-control']) !!}
                        @endif
                    </div>
                </div>
            @endfor
        </div>

        <div class="form-group">
            {!! Form::label('metro_id', 'Метро:', ['class' => 'control-label']) !!}
            {!! Form::select('metro_id', $stations, null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Адрес:', ['class' => 'control-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
            <div id="map" data-lat="{{$model->lat}}" data-lng="{{$model->lng}}"></div>
            {!! Form::hidden('lat') !!}
            {!! Form::hidden('lng') !!}
        </div>
        @if (isset($model))
            <div class="form-group">
                <img class="admin-img" src="{{$model->photo}}"/>
            </div>
        @endif
        <div class="form-group">
            {!! Form::label('photo', 'Фото:', ['class' => 'control-label']) !!}
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
            {!! HTML::linkRoute('admin.bars.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="sportEvents">
        @if (isset($model))
            @foreach($events as $event)
                <div class="form-group">
                    {!! Form::label('sportEvents' . $event->id, $event->getDescr(), ['class' => 'control-label']) !!}
                    @if (isset($model))
                        {!! Form::checkbox('events[]', $event->id, in_array($event->id, $arrModelEventsIds), ['id' => 'sportEvents' . $event->id]) !!}
                    @else
                        {!! Form::checkbox('events[]', $event->id, null, ['id' => 'sportEvents' . $event->id]) !!}
                    @endif
                </div>
            @endforeach
            <div class="form-group">
                {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
                {!! HTML::linkRoute('admin.bars.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
            </div>
        @endif
    </div>
    <div role="tabpanel" class="tab-pane" id="offers">
        @if (isset($model))
            @include('admin.offers.index', ['bar' => $model])
        @endif
    </div>
    <div role="tabpanel" class="tab-pane" id="info">
        @if (isset($model))
            @foreach($infoItems as $infoItem)
                <div class="form-group">
                    @if (isset($model))
                        {!! Form::checkbox('infoItems[]', $infoItem->id, in_array($infoItem->id, $arrInfoItemsIds), ['id' => 'infoItems' . $infoItem->id]) !!}
                    @else
                        {!! Form::checkbox('infoItems[]', $infoItem->id, null, ['id' => 'infoItems' . $infoItem->id]) !!}
                    @endif
                        {!! Form::label('infoItems' . $infoItem->id, $infoItem->name, ['class' => 'control-label', 'for' => 'infoItems' . $infoItem->id]) !!}
                </div>
            @endforeach
                <div class="form-group">
                    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
                    {!! HTML::linkRoute('admin.bars.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
                </div>
        @endif
    </div>
</div>
{!! Form::close() !!}

