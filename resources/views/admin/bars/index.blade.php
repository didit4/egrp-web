@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Бары @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Бары</h3>
        @if(!$bars->count())
            <em class="bg-warning">Ни одного бара еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.bars.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th></th>
            <th>Название</th>
            <th>Телефон</th>
            <th>Метро</th>
            <th class="address">Адрес</th>
            <th>Игры в баре</th>
            <th>Спец. офферы</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($bars as $bar)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.bars.edit', [$bar->id, '#edit']) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.bars.destroy', [$bar->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.bars.switch_active', [$bar->id]) !!}"
                           class="btn {!! $bar->active ? 'btn-primary' : 'btn-default' !!}" title="{!! $bar->active ? 'Не показывать' : 'Активировать' !!}">
                            <span class="glyphicon {!! $bar->active ? 'glyphicon-eye-open' : 'glyphicon-eye-close' !!}" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>
                    <a href="{!! URL::route('admin.bars.edit', [$bar->id]) !!}" title="Редактировать">{{ $bar->name }}</a>
                </td>
                <td>{{ $bar->phone }}</td>
                <td>{{ $bar->getMetroName() }}</td>
                <td class="address">{{ $bar->address }}</td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.bars.edit', [$bar->id, '#sportEvents']) !!}" class="btn btn-default"
                           title="Список игр">
                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                            {{ $bar->sportEvents()->count() }}
                        </a>
                        <a href="{!!  URL::route('admin.bars.edit', [$bar->id, '#sportEvents']) !!}"
                           class="btn btn-success" title="Добавить">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.bars.edit', [$bar->id, '#offers']) !!}" class="btn btn-default"
                           title="Список офферов">
                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                            {{ $bar->offers()->count() }}
                        </a>
                        <a href="{!!  URL::route('admin.bars.offers.create', [$bar->id]) !!}"
                           class="btn btn-success" title="Добавить">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
