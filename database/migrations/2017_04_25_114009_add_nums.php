<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNums extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('regions')->where('place_id', '922')->update(['num' => '27']);
		DB::table('regions')->where('place_id', '2378')->update(['num' => '95']);
		DB::table('regions')->where('place_id', '3086')->update(['num' => '08']);
		DB::table('regions')->where('place_id', '3584')->update(['num' => '04']);
		DB::table('regions')->where('place_id', '6266')->update(['num' => '67']);
		DB::table('regions')->where('place_id', '6571')->update(['num' => '69']);
		DB::table('regions')->where('place_id', '12737')->update(['num' => '65']);
		DB::table('regions')->where('place_id', '17551')->update(['num' => '33']);
		DB::table('regions')->where('place_id', '17563')->update(['num' => '76']);
		DB::table('regions')->where('place_id', '17668')->update(['num' => '78']);
		DB::table('regions')->where('place_id', '17787')->update(['num' => '45']);
		DB::table('regions')->where('place_id', '20194')->update(['num' => '46']);
		DB::table('regions')->where('place_id', '20596')->update(['num' => '06']);
		DB::table('regions')->where('place_id', '27449')->update(['num' => '21']);
		DB::table('regions')->where('place_id', '27729')->update(['num' => '72']);
		DB::table('regions')->where('place_id', '27772')->update(['num' => '44']);
		DB::table('regions')->where('place_id', '30372')->update(['num' => '47']);
		DB::table('regions')->where('place_id', '31317')->update(['num' => '09']);
		DB::table('regions')->where('place_id', '31464')->update(['num' => '74']);
		DB::table('regions')->where('place_id', '32280')->update(['num' => '48']);
		DB::table('regions')->where('place_id', '35236')->update(['num' => '41']);
		DB::table('regions')->where('place_id', '35516')->update(['num' => '73']);
		DB::table('regions')->where('place_id', '39541')->update(['num' => '07']);
		DB::table('regions')->where('place_id', '41072')->update(['num' => '71']);
		DB::table('regions')->where('place_id', '42518')->update(['num' => '43']);
		DB::table('regions')->where('place_id', '42892')->update(['num' => '29']);
		DB::table('regions')->where('place_id', '45220')->update(['num' => '50']);
		DB::table('regions')->where('place_id', '46416')->update(['num' => '28']);
		DB::table('regions')->where('place_id', '49113')->update(['num' => '42']);
		DB::table('regions')->where('place_id', '49937')->update(['num' => '13']);
		DB::table('regions')->where('place_id', '50857')->update(['num' => '77']);
		DB::table('regions')->where('place_id', '52563')->update(['num' => '23']);
		DB::table('regions')->where('place_id', '53957')->update(['num' => '25']);
		DB::table('regions')->where('place_id', '56620')->update(['num' => '35']);
		DB::table('regions')->where('place_id', '57088')->update(['num' => '82']);
		DB::table('regions')->where('place_id', '61386')->update(['num' => '15']);
		DB::table('regions')->where('place_id', '63777')->update(['num' => '64']);
		DB::table('regions')->where('place_id', '68481')->update(['num' => '18']);
		DB::table('regions')->where('place_id', '70826')->update(['num' => '26']);
		DB::table('regions')->where('place_id', '72945')->update(['num' => '31']);
		DB::table('regions')->where('place_id', '76497')->update(['num' => '37']);
		DB::table('regions')->where('place_id', '78819')->update(['num' => '01']);
		DB::table('regions')->where('place_id', '79983')->update(['num' => '24']);
		DB::table('regions')->where('place_id', '80913')->update(['num' => '16']);
		DB::table('regions')->where('place_id', '81907')->update(['num' => '89']);
		DB::table('regions')->where('place_id', '84523')->update(['num' => '12']);
		DB::table('regions')->where('place_id', '85072')->update(['num' => '30']);
		DB::table('regions')->where('place_id', '87474')->update(['num' => '87']);
		DB::table('regions')->where('place_id', '87885')->update(['num' => '19']);
		DB::table('regions')->where('place_id', '89463')->update(['num' => '66']);
		DB::table('regions')->where('place_id', '89643')->update(['num' => '17']);
		DB::table('regions')->where('place_id', '92335')->update(['num' => '49']);
		DB::table('regions')->where('place_id', '94190')->update(['num' => '02']);
		DB::table('regions')->where('place_id', '95138')->update(['num' => '83']);
		DB::table('regions')->where('place_id', '98188')->update(['num' => '79']);
		DB::table('regions')->where('place_id', '102912')->update(['num' => '40']);
		DB::table('regions')->where('place_id', '105762')->update(['num' => '68']);
		DB::table('regions')->where('place_id', '107846')->update(['num' => '32']);
		DB::table('regions')->where('place_id', '108143')->update(['num' => '10']);
		DB::table('regions')->where('place_id', '108276')->update(['num' => '70']);
		DB::table('regions')->where('place_id', '118226')->update(['num' => '22']);
		DB::table('regions')->where('place_id', '126049')->update(['num' => '36']);
		DB::table('regions')->where('place_id', '126542')->update(['num' => '39']);
		DB::table('regions')->where('place_id', '127088')->update(['num' => '86']);
		DB::table('regions')->where('place_id', '127754')->update(['num' => '05']);
		DB::table('regions')->where('place_id', '130311')->update(['num' => '11']);
		DB::table('regions')->where('place_id', '133079')->update(['num' => '14']);
		DB::table('regions')->where('place_id', '137973')->update(['num' => '92']);
		DB::table('regions')->where('place_id', '138651')->update(['num' => '38']);
		DB::table('regions')->where('place_id', '139837')->update(['num' => '34']);
		DB::table('regions')->where('place_id', '140372')->update(['num' => '75']);
		DB::table('regions')->where('place_id', '140935')->update(['num' => '03']);
		DB::table('regions')->where('place_id', '142016')->update(['num' => '54']);
		DB::table('regions')->where('place_id', '142017')->update(['num' => '60']);
		DB::table('regions')->where('place_id', '142018')->update(['num' => '62']);
		DB::table('regions')->where('place_id', '142019')->update(['num' => '63']);
		DB::table('regions')->where('place_id', '142020')->update(['num' => '51']);
		DB::table('regions')->where('place_id', '142021')->update(['num' => '52']);
		DB::table('regions')->where('place_id', '142022')->update(['num' => '53']);
		DB::table('regions')->where('place_id', '142023')->update(['num' => '55']);
		DB::table('regions')->where('place_id', '142024')->update(['num' => '56']);
		DB::table('regions')->where('place_id', '142025')->update(['num' => '57']);
		DB::table('regions')->where('place_id', '142026')->update(['num' => '58']);
		DB::table('regions')->where('place_id', '142027')->update(['num' => '59']);
		DB::table('regions')->where('place_id', '142028')->update(['num' => '61']);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::table('regions')->update(['num' => 0]);
	}

}
