<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWatch extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reqs', function($table)
		{
			$table->timestamp('watched_at');
			$table->integer('user_watch_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reqs', function($table)
		{
			$table->dropColumn('watched_at');
			$table->dropColumn('user_watch_id');
		});
	}

}
