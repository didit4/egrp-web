<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('st_type', function(Blueprint $table)
        {
            $table->integer('sort');
            $table->boolean('disable');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('st_type', function(Blueprint $table)
        {
            $table->dropColumn('sort');
            $table->dropColumn('disable');
        });
	}

}
