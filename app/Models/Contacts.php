<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Contacts extends Model
{

    protected $table = 'contacts';
    public $timestamps = false;

    protected $fillable = [
        'text',
        'email',
    ];

    public static $rules = [
        'text' => 'required',
    ];

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
//        $this->text = preg_replace('/<style(.*)/m', '', $this->text);
//        $this->text = str_replace('</style>', '', $this->text);
        if ($this->id == 1) {
            return [
                'text' => strip_tags($this->text),
                'email' => $this->email,
            ];
        } else {
            return [
                'text' => strip_tags($this->text)
            ];
        }
    }
}
