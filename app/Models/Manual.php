<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Manual extends Model
{
    protected $table = 'manuals';
    public $timestamps = false;

    protected $fillable = [
        'text',
    ];

    public static $rules = [
        'text' => 'required',
    ];

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->id,
            'text' => (string) $this->text
        ];
    }

}
