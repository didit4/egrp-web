<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Question extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'email',
        'text',
        'answer',
        'is_send'
    ];

    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'text' => 'required',
    ];

    public static $rulesAnswer = [
        'answer' => 'required',
    ];

}
