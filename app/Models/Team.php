<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

class Team extends Model implements \JsonSerializable
{

    protected $fillable = [
        'name',
        'logo',
        'active',
        'fav',
        'sort',
    ];

    public static $rules = array(
        'name' => 'required',
    );

    const MAX_TEAMS = 6;

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    public function save(array $options = array())
    {
        if (Input::hasFile('logo')) {

            $file = Input::file('logo');
            $fpath = base_path() . '/public/static/img/teams/';

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fname = sha1($filename . time()) . '.' . $extension;

            $file->move($fpath, $fname);
            $this->logo = '/img/teams/' . $fname;
        }

        parent::save($options);
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => $this->getLogo(),
        ];
    }

    public static function getMaxTeams()
    {
        return self::MAX_TEAMS;
    }

    public function getLogo()
    {
        return $this->logo ? Config::get('app.url') . $this->logo : Config::get('app.url') . '/img/logo.png';
    }
}
