<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class ReqType extends Model
{
    protected $table = 'req_types';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'order',
    ];

    public static $rules = [
        'name' => 'required',
    ];

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->id,
            'name' => (string) $this->name,
            'order' => (string) $this->order,
        ];
    }
}
