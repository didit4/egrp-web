<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

    protected $fillable = [
        'text',
        'name',
    ];

    public static $rules = array(
        'name' => 'required',
    );

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->id,
            'name' => $this->name,
            'text' =>  $this->name . ' ' . $this->text
        ];
    }
}
