<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

class CatalogItem extends Model {

    protected $fillable = [
        'name',
        'img',
        'descr',
    ];

    public static $rules = [
        'name' => 'required',
    ];

    public $active;

    /**
     * Get the catalog record associated with the item.
     */
    public function catalog()
    {
        return $this->hasOne('App\Models\Catalog');
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }


    public function saveFile()
    {
        if (Input::hasFile('img_tmp')) {

            $file = Input::file('img_tmp');
            $fpath = base_path() . '/public/static/img/catalogs/';

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fname = sha1($filename . time()) . '.' . $extension;

            $this->img = '/img/catalogs/' . $fname;

            $file->move($fpath, $fname);
        }

        return $this;

    }

    public function getImg()
    {
        return Config::get('app.url') . $this->img;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name,
        ];
        if ($this->img) {
            $result['img'] = $this->getImg();
        }
        if ($this->descr) {
            $result['descr'] = $this->descr;
        }
        if (isset($this->active)) {
            $result['active'] = $this->active;
        }
        return $result;
    }

}
