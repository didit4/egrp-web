<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model {

    protected $fillable = [
        'name',
    ];

    public static $rules = [
        'name' => 'required',
    ];

    /**
     * Get the items for the catalog.
     */
    public function items()
    {
        return $this->hasMany('App\Models\CatalogItem');
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    public function getListForDropdown($emptyItem = false)
    {
        $items = $this->items->lists('name', 'id');
        $stations = [];
        if ($emptyItem) {
            $stations[''] = $emptyItem;
        }
        foreach ($items as $key => $item) {
            $stations[$key] = $item;
        }
        return $stations;
    }
}
