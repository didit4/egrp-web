<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Rule extends Model
{

    protected $table = 'rules';

    protected $fillable = [
        'text',
        'title1',
        'title2',
        'list1',
        'list2',
    ];

    public static $rules = [
//        'text' => 'required',
        'title1' => 'required',
        'title2' => 'required',
//        'list1' => 'required',
//        'list2' => 'required',
    ];

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'text' => $this->geMainText(),
            'title1' => $this->title1,
            'title2' => $this->title2,
            'list1' => $this->getList1(),
            'list2' => $this->getList2(),
        ];
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    public function getList1()
    {
        return unserialize($this->list1);
    }

    public function getList2()
    {
        return unserialize($this->list2);
    }

    public function geMainText()
    {
        return unserialize($this->text);
    }

    public function save(array $options = array())
    {
        $this->list1 = serialize(Input::get('arrList1', []));
        $this->list2 = serialize(Input::get('arrList2', []));
        $this->text = serialize(Input::get('arrList3', []));

        parent::save($options);
    }
}
