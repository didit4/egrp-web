<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'password', 'email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	protected $have_role;

	public static $rules = array(
		'name' => 'required|min:3',
		'email' => 'required|min:3',
		'password' => 'required|min:3',
	);

	public static $rulesUpdate = array(
		'name' => 'required|min:3',
	);

	public function role()
	{
		return $this->hasOne('App\Models\Role', 'id', 'role_id');
	}

	public function teams()
	{
		return $this->belongsToMany('App\Models\Team', 'user_teams', 'user_id', 'team_id');
	}

	public function pointsLog()
	{
		return $this->hasMany('App\Models\Point');
	}

	public function hasRole($roles)
	{
		$this->have_role = $this->getUserRole();
		if ($this->have_role === null) {
			return false;
		}
		// Check if the user is a root account
		if($this->have_role->name == 'Root') {
			return true;
		}
		if(is_array($roles)){
			foreach($roles as $need_role){
				if($this->checkIfUserHasRole($need_role)) {
					return true;
				}
			}
		} else{
			return $this->checkIfUserHasRole($roles);
		}
		return false;
	}
	private function getUserRole()
	{
		return $this->role()->getResults();
	}
	private function checkIfUserHasRole($need_role)
	{
		return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
	}

	public function roleName()
	{
		$role = $this->role()->getResults();
		if ($role !== null) {
			return $role->name;
		}
		return '';
	}

	public function isAdmin()
	{
		return $this->hasRole('Administrator');
	}

	public function isMngr()
	{
		return $this->hasRole('Manager');
	}

	/**
	 * @param $team Team
	 * @return $this
	 */
	public function addFavoriteTeam($team)
	{
		$this->teams()->save($team);
		return $this;
	}

	/**
	 * @param $team_id int
	 * @return $this
	 */
	public function removeFavoriteTeam($team_id)
	{
		$this->teams()->detach($team_id);
		return $this;
	}

	/**
	 * @param $team Team
	 * @return boolean
	 */
	public function hasFavoriteTeam($team)
	{
		return $this->teams()->getResults()->contains($team);
	}

	public function getRoleName()
	{
		$role = $this->role()->getResults();
		if ($role !== null) {
			return $role->name;
		}
		return '';
	}

	/**
	 * @return array
	 */
	function jsonSerialize()
	{
		$teams = $this->teams()->getResults();
		$arrTeams = [];
		foreach ($teams as $team) {
			$arrTeams[] = $team;
		}
		$points = $this->pointsLog()->orderBy('id', 'desc')->take(10)->getResults();
		$arrPoints = [];
		foreach ($points as $point) {
			$arrPoints[] = $point;
		}
		return [
			'id' => $this->id,
			'name' => $this->name,
			'photo' => $this->photo,
			'points' => $this->points,
			'teams' => $arrTeams,
			'pointsLog' => $arrPoints,
		];
	}
}
