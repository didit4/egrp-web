<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Faq extends Model
{

    protected $table = 'faq';
    public $timestamps = false;

    protected $fillable = [
        'question',
        'answer',
        'active',
    ];

    public static $rules = [
        'question' => 'required',
        'answer' => 'required',
    ];

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->id,
            'question' => $this->question,
            'answer' => $this->answer,
            'plus' => (string) $this->plus,
            'minus' => (string) $this->minus,
            'sort' => (string) $this->sort
        ];
    }
}
