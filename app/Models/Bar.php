<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Config;

class Bar extends Model
{

    protected $fillable = [
        'name',
        'phone',
        'photo',
        'address',
        'metro_id',
        'offer_id',
        'lat',
        'lng',
        'schedule',
        'active',
    ];

    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'lat' => 'required',
        'lng' => 'required',
    ];

    public static $weeksDays = [
        'пн',
        'вт',
        'ср',
        'чт',
        'пт',
        'сб',
        'вс',
    ];

    public function sportEvents()
    {
        return $this->belongsToMany('App\Models\SportEvent', 'bar_events', 'bar_id', 'event_id');
    }

    public function info()
    {
        $infos = DB::table('bar_infos')
            ->where('bar_id', $this->id)
            ->get();

        $arrIds = [];
        foreach ($infos as $info) {
            $arrIds[] = $info->info_id;
        }
        $infoObjs = CatalogItem::whereIn('id', $arrIds)->get();
        foreach ($infoObjs as $infoObj) {
            foreach ($infos as $info) {
                if ($info->info_id === $infoObj->id)
                $infoObj->active = $info->active;
            }
        }
        return $infoObjs;
    }

    public function metro()
    {
        return $this->hasOne('App\Models\CatalogItem', 'id', 'metro_id');
    }

    public function offers()
    {
        return $this->hasMany('App\Models\Offer');
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    public function save(array $options = array())
    {
        if (Input::hasFile('photo')) {

            $file = Input::file('photo');
            $fpath = base_path() . '/public/static/img/bars/';

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fname = sha1($filename . time()) . '.' . $extension;

            $file->move($fpath, $fname);
            $this->photo = '/img/bars/' . $fname;
        }
        if ($this->id) {
            $this->sportEvents()->sync(Input::get('events', []));
            $infoItems = Catalog::where('name', 'Услуги и удобства в барах')->first()->items()->getResults();
            $formInfoItems = array_flip(Input::get('infoItems', []));
            $insertInfos = [];
            foreach ($infoItems as $infoItem) {
                $insertInfos[] = [
                    'bar_id' => $this->id,
                    'info_id' => $infoItem->id,
                    'active' => isset($formInfoItems[$infoItem->id])
                ];
            }
            DB::table('bar_infos')
                ->where('bar_id', $this->id)
                ->delete();
            DB::table('bar_infos')->insert($insertInfos);
        }
        $this->schedule = serialize(Input::get('schedule', []));

        parent::save($options);
    }

    public function getMetroName()
    {
        return $this->metro['name'];
    }

    public function getSchedule()
    {
        return unserialize($this->schedule);
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $offers = $this->offers()->getResults();
        $arrOffers = [];
        foreach ($offers as $offer) {
            $arrOffers[] = $offer;
        }
        $info = $this->info();
        $arrInfo = [];
        foreach ($info as $_info) {
            $arrInfo[] = $_info;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'photo' => $this->photo ? Config::get('app.url') . $this->photo : '',
            'phone' => $this->phone,
            'address' => $this->address,
            'coords' => [
                'lat' => $this->lat,
                'lng' => $this->lng,
            ],
            'metro' => $this->metro()->getResults(),
            'offers' => $arrOffers,
            'info' => $arrInfo
        ];
    }
}
