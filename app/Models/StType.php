<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class StType extends Model
{
    protected $table = 'st_type';

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->id,
            'name' => (string) $this->name,
            'sort' => (string) $this->sort,
            'disable' => (string) $this->disable
        ];
    }
}
