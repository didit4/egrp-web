<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class PlaceType extends Model
{
    protected $table = 'place_type_names';
}
