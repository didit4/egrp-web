<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Place extends Model
{

    public function placeType()
    {
        return $this->hasOne('App\Models\PlaceType', 'place_type_name_id', 'place_type_name_id');
    }

    public function getFullName()
    {
        if ($this->type == 3) {
            return $this->placeType->name . '. ' . $this->name;
        }
        if ($this->type == 2) {
            return $this->name;
        }
        return $this->placeType->after_place_name ? $this->name . ' ' . $this->placeType->full_name : $this->placeType->full_name . ' ' . $this->name;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->place_id,
            'name' => $this->getFullName()
        ];
    }
}
