<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Setting extends Model
{

    protected $table = 'settings';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'value',
    ];

    public static $rules = [
        'value' => 'required',
    ];

}
