<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\Faq;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Validator;

class FaqController extends BaseController
{

    public function get()
    {
        $faqs = Faq::where('active', true)->orderBy('sort', 'asc')->get();
        $result = [];
        foreach ($faqs as $faq) {
            $result[] = $faq;
        }
        return response()->json(['faqs' => $result], 200);
    }

}
