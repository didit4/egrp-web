<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\Team;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use JWTAuth;

class TeamController extends BaseController
{

    public function all()
    {
        $teams = Team::where('active', true)->orderBy('fav', 'desc')->orderBy('sort', 'asc')->orderBy('name', 'asc')->get();
        $result = [];
        foreach ($teams as $team) {
            $result[] = $team;
        }
        return response()->json(['teams' => $result], 200);
    }

    public function postFavorite($id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $user = JWTAuth::parseToken()->authenticate();
        $team = Team::find($id);
        $team !== NULL or abort(404, 'Team with id #' . $id . ' - not found');
        //проверка на уже фаворита
        $user->addFavoriteTeam($team);
        return response()->json([], 201);
    }

    public function deleteFavorite($id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $user = JWTAuth::parseToken()->authenticate();
        $team = Team::find($id);
        $team !== NULL or abort(404, 'Team with id #' . $id . ' - not found');
        $user->removeFavoriteTeam($id);
        return response()->json([], 204);
    }

}
