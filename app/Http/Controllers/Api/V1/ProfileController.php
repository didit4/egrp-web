<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\Bar;
use App\Models\Point;
use App\Models\SportEvent;
use App\Models\Team;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use JWTAuth;
use Illuminate\Http\Request;
use DB;
use Lang;

class ProfileController extends BaseController
{

    public function points(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }

        if (!$request->input('val') || !$request->input('descr')) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.points_error')
            ], 422);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $point = Point::create([
            'val' => $request->input('val'),
            'descr' => $request->input('descr'),
            'user_id' => $user->id,
        ]);
        $userPointsSum = DB::table('points')->where('user_id', $user->id)->sum('val');
        $user->points = $userPointsSum;
        $user->save();
        return response()->json(['points' => $userPointsSum], 200);
    }

    public function data()
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }

        $user = JWTAuth::parseToken()->authenticate();
        return response()->json([
            'profile' => $user
        ], 200);
    }

    public function sync(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }

        $mergeData = $request->only(['events', 'teams']);
        $valid = [
            'events' => 'required',
            'teams' => 'required',
        ];
        $validator = Validator::make($mergeData, $valid);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.sync_error')
            ], 422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        if (count($mergeData['teams'])) {
            foreach ($mergeData['teams'] as $teamId) {
                $team = Team::find($teamId);

                if ($team === null || $user->hasFavoriteTeam($team)) {
                    continue;
                }
                $user->addFavoriteTeam($team);
                if (count($user->teams()->getResults()) >= Team::getMaxTeams()) {
                    break;
                }
            }

        }

        if (count($mergeData['events'])) {
            foreach ($mergeData['events'] as $event) {
                $eventUser = DB::table('events_brars_usres_status')
                    ->where('user_id', $user->id)
                    ->where('event_id', $event['event'])
                    ->first();
                if ($eventUser !== null) {
                    continue;
                }

                $eventObj = SportEvent::find($event['event']);
                $barObj = Bar::find($event['bar']);
                if ($eventObj === null || $barObj === null) {
                    continue;
                }

                DB::table('events_brars_usres_status')->insert([
                    'event_id' => $event['event'],
                    'bar_id' => $event['bar'],
                    'user_id' => $user->id,
                    'status' => $event['status'],
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                ]);
            }

        }
    }

}
