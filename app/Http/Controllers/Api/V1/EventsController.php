<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\Bar;
use App\Models\SportEvent;
use App\Models\Team;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use DB;

class EventsController extends BaseController
{

    public function all(Request $request)
    {
        if ($request->header('authorization')) {
            if (!$token = JWTAuth::parseToken()) {
                return $this->response->errorUnauthorized();
            } else {
                $user = JWTAuth::parseToken()->authenticate();
                $usersEventsIds = DB::table('events_brars_usres_status')
                    ->where('user_id', $user->id)
                    ->lists('event_id');
            }
        } else {
            $usersEventsIds = $request->input('events_ids') ? : [];
        }

        $events = SportEvent::where('time', '>=', strtotime(date("Y/m/d h:i:s", strtotime('-120 minutes', time()))))
            ->whereNotIn('id', $usersEventsIds)
            ->where('active', true)
            ->orderBy('time', 'asc')
            ->get();
        $userEvents = SportEvent::where('time', '>=', strtotime(date("Y/m/d h:i:s", strtotime('-120 minutes', time()))))
            ->whereIn('id', $usersEventsIds)
            ->where('active', true)
            ->orderBy('time', 'asc')
            ->get();
        $arrEvents = [];
        foreach ($events as $event) {
            $arrEvents[] = $event;
        }
        $arrUserEvents = [];
        foreach ($userEvents as $event) {
            $event->iwillgo = true;
            $arrUserEvents[] = $event;
        }
        return response()->json([
            'events' => $arrEvents,
            'userEvents' => $arrUserEvents,
        ], 200);
    }

    public function favorites(Request $request)
    {
        if ($request->header('authorization')) {
            if (!$token = JWTAuth::parseToken()) {
                return $this->response->errorUnauthorized();
            } else {
                $user = JWTAuth::parseToken()->authenticate();
                $teamsIds = $user->teams()->getResults()->lists('id');

                $usersEventsIds = DB::table('events_brars_usres_status')
                    ->where('user_id', $user->id)
                    ->lists('event_id');
            }
        } else {
            $teamsIds = $request->input('teams_ids') ? : [];
            $usersEventsIds = $request->input('events_ids') ? : [];
        }

        $events = SportEvent::where('time', '>=', strtotime(date("Y/m/d h:i:s", strtotime('-120 minutes', time()))));
        if (count($teamsIds)) {
            $events = $events->whereIn('team1_id', $teamsIds)
                ->orWhere(function($query) use($teamsIds)
                {
                    $query->whereIn('team2_id', $teamsIds);
                });
        }
        $events = $events->whereNotIn('id', $usersEventsIds)
            ->orderBy('time', 'asc')
            ->where('active', true)
            ->get();

        $userEvents = SportEvent::where('time', '>=', strtotime(date("Y/m/d h:i:s", strtotime('-120 minutes', time()))))
            ->whereIn('id', $usersEventsIds)
            ->where('active', true)
            ->orderBy('time', 'asc')
            ->get();

        $arrEvents = [];
        foreach ($events as $event) {
            $arrEvents[] = $event;
        }
        $arrUserEvents = [];
        foreach ($userEvents as $event) {
            $event->iwillgo = true;
            $arrUserEvents[] = $event;
        }
        return response()->json([
            'events' => $arrEvents,
            'userEvents' => $arrUserEvents,
        ], 200);
    }

    public function bars(Request $request, $id)
    {
        if ($request->header('authorization')) {
            if (! $token = JWTAuth::parseToken()) {
                return $this->response->errorUnauthorized();
            }
        }

        $event = SportEvent::find($id);

        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');
        $bars = $event->bars()->where('active', true)->getResults();
        $result = [];
        foreach ($bars as $bar) {
            $result[] = $bar;
        }
        return response()->json([
            'event' => $event,
            'bars' => $result
        ], 200);
    }

    public function bar($id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $event = SportEvent::find($id);
        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');

        $user = JWTAuth::parseToken()->authenticate();
        $barEvent = DB::table('events_brars_usres_status')
            ->where('user_id', $user->id)
            ->where('event_id', $id)
            ->first();
        $barEvent !== NULL or abort(404, 'Users #' . $user->id . ' Bar Event with id #' . $id . ' - not found');

        $bar = Bar::find($barEvent->bar_id);
        $bar !== NULL or abort(404, 'Bar with id #' . $barEvent->bar_id . ' - not found');

        $response = [
            'bar' => $bar,
            'status' => $barEvent->status
        ];

        if (SportEvent::STATUS_IHERE === $barEvent->status) {
            $response['was_here'] = 'Вы были здесь ' . date('d.m', strtotime($barEvent->updated_at));
            $pointsSum = DB::table('points')
                ->where('user_id', $user->id)
                ->whereDate('created_at', '=', date('Y-m-d', strtotime($barEvent->updated_at)))
                ->sum('val');
            if ($pointsSum) {
                $response['got_points'] = 'и получили в этот день ' . $pointsSum . ' баллов, обменяйте их на бонусы 01.09';
            }
        }

        return response()->json($response, 200);
    }

    public function iwillgo($id, $bar_id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $event = SportEvent::find($id);
        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');
        $bar = Bar::find($bar_id);
        $bar !== NULL or abort(404, 'Bar with id #' . $bar_id . ' - not found');

        $user = JWTAuth::parseToken()->authenticate();

        $eventBar = DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('user_id', $user->id)
            ->first();

        $eventBar === NULL or abort(404, 'EventBar exist');

        DB::table('events_brars_usres_status')->insert([
            'event_id' => $id,
            'bar_id' => $bar_id,
            'user_id' => $user->id,
            'status' => SportEvent::STATUS_IWILLGO,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        return response()->json([], 201);
    }

    public function ihere($id, $bar_id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $event = SportEvent::find($id);
        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');
        $bar = Bar::find($bar_id);
        $bar !== NULL or abort(404, 'Bar with id #' . $bar_id . ' - not found');

        $user = JWTAuth::parseToken()->authenticate();

        $eventBar = DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('bar_id', $bar_id)
            ->where('user_id', $user->id)
            ->first();

        $eventBar !== NULL or abort(404, 'EventBar not found');
        //TODO: проверка, если пользователь уже здесь
        DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('bar_id', $bar_id)
            ->where('user_id', $user->id)
            ->update([
            'status' => SportEvent::STATUS_IHERE,
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        return response()->json([], 204);
    }

    public function iwilltgo($id, $bar_id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $event = SportEvent::find($id);
        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');
        $bar = Bar::find($bar_id);
        $bar !== NULL or abort(404, 'Bar with id #' . $bar_id . ' - not found');

        $user = JWTAuth::parseToken()->authenticate();

        DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('bar_id', $bar_id)
            ->where('user_id', $user->id)
            ->delete();

        return response()->json([], 204);
    }

    public function send_number(Request $request, $id, $bar_id)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $event = SportEvent::find($id);
        $event !== NULL or abort(404, 'Event with id #' . $id . ' - not found');
        $bar = Bar::find($bar_id);
        $bar !== NULL or abort(404, 'Bar with id #' . $bar_id . ' - not found');

        $user = JWTAuth::parseToken()->authenticate();

        $eventBar = DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('bar_id', $bar_id)
            ->where('user_id', $user->id)
            ->first();

        $eventBar !== NULL or abort(404, 'EventBar not found');

        DB::table('events_brars_usres_status')
            ->where('event_id', $id)
            ->where('bar_id', $bar_id)
            ->where('user_id', $user->id)
            ->update([
                'num' => $request->input('num'),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

        return response()->json([], 204);
    }

}
