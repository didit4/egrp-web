<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Catalog;
use Illuminate\Support\Facades\Response;
use Validator;

class CatalogController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $catalogs = Catalog::all();
        return view('admin.catalogs.index', compact('catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.catalogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Catalog::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.catalogs.create')
                ->withErrors($validator)
                ->withInput();
        }

        Catalog::create($input);

        return redirect()->route('admin.catalogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = Catalog::find($id);
        return view('admin.catalogs.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Catalog::find($id);
        if($model === null){
            return redirect('admin/catalogs');
        }
        return view('admin.catalogs.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Catalog::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.catalogs.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Catalog::find($id);
        $model->update($input);
        return redirect('admin/catalogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Catalog::find($id)->delete();
        return response()->json();
    }

}
