<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Bar;
use App\Models\Catalog;
use App\Models\PayType;
use App\Models\Setting;
use App\Models\SportEvent;
use Illuminate\Support\Facades\Response;
use Validator;

class PayTypeController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $types = PayType::all();
        return view('admin.paytypes.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.paytypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, PayType::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.paytypes.index')
                ->withErrors($validator)
                ->withInput();
        }

        PayType::create($input);

        return redirect()->route('admin.paytypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = PayType::find($id);
        return view('admin.paytypes.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = PayType::find($id);
        if($model === null){
            return redirect('admin/paytypes');
        }
        return view('admin.paytypes.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, PayType::$rules);
        if ($validator->fails()) {
            return response()->json(['errors' => 'Ошибка валидации']);
        }

        $model = PayType::find($id);
        $model->update($input);
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Bar::find($id)->delete();
        return response()->json();
    }

}
