<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Bar;
use App\Models\Catalog;
use App\Models\Contacts;
use App\Models\Faq;
use App\Models\SportEvent;
use Illuminate\Support\Facades\Response;
use Validator;

class ContactsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $contact = Contacts::first();
        return view('admin.contacts.index', compact('contact'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function offer()
    {
        $contact = Contacts::where('id', 2)->first();
        return view('admin.contacts.offer', compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Contacts::$rules);

        if ($validator->fails()) {
            return redirect()->route('admin.contacts.create')
                ->withErrors($validator)
                ->withInput();
        }

        Contacts::create($input);

        return redirect()->route('admin.contacts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Contacts::find($id);
        if($model === null){
            return redirect('admin/contacts');
        }
        return view('admin.contacts.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Contacts::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.contacts.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Contacts::find($id);
        $model->update($input);
        if ($id == 1) {
            return redirect('admin/contacts');
        } else {
            return redirect()->route('admin.contacts.offer');
        }
    }

}
