<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Bar;
use App\Models\Catalog;
use App\Models\Faq;
use App\Models\SportEvent;
use Illuminate\Support\Facades\Response;
use Validator;

class FaqController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $faqs = Faq::paginate(10);
        return view('admin.faqs.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.faqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Faq::$rules);

        if ($validator->fails()) {
            return redirect()->route('admin.faqs.create')
                ->withErrors($validator)
                ->withInput();
        }

        Faq::create($input);

        return redirect()->route('admin.faqs.index');
    }

    public function switchActive($id)
    {
        $faq = Faq::find($id);
        if ($faq) {
            $faq->active = !$faq->active;
            $faq->save();
        }
        return redirect()->route('admin.faqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = Faq::find($id);
        return view('admin.faqs.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Faq::find($id);
        return view('admin.faqs.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Faq::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.faqs.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Faq::find($id);
        $model->update($input);
        return redirect('admin/faqs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Faq::find($id)->delete();
        return response()->json();
    }

}
