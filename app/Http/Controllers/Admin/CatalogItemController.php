<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\CatalogItem;
use App\Models\Catalog;
use Illuminate\Support\Facades\Response;
use Validator;
use DB;

class CatalogItemController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $catalogs = CatalogItem::all();
        return view('admin.catalogs.index', compact('catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Catalog $catalog
     * @return Response
     */
    public function create($catalog)
    {
        return view('admin.catalogs.items.create', compact('catalog'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Catalog $catalog
     * @return Response
     */
    public function store($catalog)
    {
        $catalog = Catalog::find($catalog);
        if ($catalog) {
            $input = $this->request->all();

            $validator = Validator::make($input, CatalogItem::$rules);
            if ($validator->fails()) {
                return redirect()->route('admin.catalogs.items.create', [$catalog->id])
                    ->withErrors($validator)
                    ->withInput();
            }
            $item = CatalogItem::create($input);
            $catalog->items()->save($item);
            $item->saveFile()->save();
        }
        return redirect()->route('admin.catalogs.show', [$catalog->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Catalog $catalog
     * @param  int $id
     * @return Response
     */
    public function edit($catalog, $id)
    {
        $catalog = Catalog::find($catalog);
        if ($catalog) {
            $model = CatalogItem::find($id);
            if ($model === null) {
                return redirect()->route('admin.catalogs.show', [$catalog->id]);
            }
        }
        return view('admin.catalogs.items.edit', compact('model', 'catalog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Catalog $catalog
     * @param  int $id
     * @return Response
     */
    public function update($catalog, $id)
    {
        $catalog = Catalog::find($catalog);
        if ($catalog) {
            $input = $this->request->all();

            $validator = Validator::make($input, CatalogItem::rules($id));
            if ($validator->fails()) {
                return redirect()->route('admin.catalogs.items.edit', [$catalog->id, $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            $model = CatalogItem::find($id);
            $model->update($input);
            $model->saveFile()->save();
        }
        return redirect()->route('admin.catalogs.show', [$catalog->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Catalog $catalog
     * @param  int $id
     * @return Response
     */
    public function destroy($catalog, $id)
    {
        $result = DB::table('bar_infos')->where('info_id', $id)->first();

        if ($result) {
            return response()->json(['error' => true, 'message' => 'Не возможно удалить, услуга прикреплена к барам']);
        } else {
            CatalogItem::find($id)->delete();
            return response()->json();
        }
    }

}
