<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Team;
use Illuminate\Support\Facades\Response;
use Validator;

class TeamController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $teams = Team::orderBy('fav', 'desc')->orderBy('sort', 'asc')->orderBy('name', 'asc')->get();
        return view('admin.teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Team::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.teams.create')
                ->withErrors($validator)
                ->withInput();
        }

        Team::create($input);

        return redirect()->route('admin.teams.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = Team::find($id);
        return view('admin.teams.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Team::find($id);
        if($model === null){
            return redirect('admin/teams');
        }
        return view('admin.teams.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Team::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.teams.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Team::find($id);
        $model->update($input);
        return redirect('admin/teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Team::find($id)->delete();
        return response()->json();
    }

    public function switch_active($id)
    {
        $team = Team::find($id);
        if($team === null){
            return redirect('admin/teams');
        }
        $team->active = !$team->active;
        $team->save();
        return redirect('admin/teams');
    }

    public function fav($id)
    {
        $team = Team::find($id);
        if($team === null){
            return redirect('admin/teams');
        }
        $team->fav = !$team->fav;
        $team->save();
        return redirect('admin/teams');
    }

    public function reorder()
    {
        $items = $this->request->all()['items'];
        foreach ($items as $k => $v) {
            Team::where('id', $v)->update(['sort' => $k]);
        }
    }

}
