<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Contacts;
use App\Models\Manual;
use Illuminate\Support\Facades\Response;
use Validator;

class ManualController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $manual = Manual::first();
        return view('admin.manuals.index', compact('manual'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.manuals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Manual::$rules);

        if ($validator->fails()) {
            return redirect()->route('admin.manuals.create')
                ->withErrors($validator)
                ->withInput();
        }

        Manual::create($input);

        return redirect()->route('admin.manuals.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Manual::find($id);
        if($model === null){
            return redirect('admin/manuals');
        }
        return view('admin.manuals.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Manual::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.manuals.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Manual::find($id);
        $model->update($input);
        if ($id == 1) {
            return redirect('admin/manuals');
        } else {
            return redirect()->route('admin.manuals.offer');
        }
    }

}
