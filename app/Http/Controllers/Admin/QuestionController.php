<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Question;
use Mail;
use Illuminate\Support\Facades\Input;


class QuestionController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $questions = Question::orderBy('id', 'DESC')->paginate(10);
        return view('admin.questions.index', compact('questions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Question::find($id);

        return view('admin.questions.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $model = Question::find($id);
        $model->update($input);
        Mail::send('emails.question', ['model' => $model], function ($message) use($model) {

            $message->from('hello@egrp.today', 'egrp.today');
            $message->to($model->email)->subject('Ответ на ваш вопрос с egrp.today');

        });
        $model->is_send = true;
        $model->save();
        return redirect()->route('admin.questions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Question::find($id)->delete();
        return response()->json();
    }


}
