<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Models\Contacts;

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'middleware' => ['auth', 'roles'],
	'roles' => ['administrator', 'manager'],
], function () {
	# Admin users
	Route::resource('users', 'UserController');
	Route::resource('settings', 'SettingController');
	# Admin bars
	Route::get('/faqs/{id}/switchActive', ['uses' => 'FaqController@switchActive', 'as' => 'admin.faqs.switchActive']);
	Route::resource('faqs', 'FaqController');
	Route::get('/reqs/statistic', ['uses' => 'ReqController@statistic', 'as' => 'admin.reqs.statistic']);
	Route::get('/reqs/change_month', ['uses' => 'ReqController@change_month', 'as' => 'admin.reqs.change_month']);
	Route::get('/reqs/excel', ['uses' => 'ReqController@excel', 'as' => 'admin.reqs.excel']);
	Route::get('/reqs/search', ['uses' => 'ReqController@search', 'as' => 'admin.reqs.search']);
	Route::get('/reqs/switch_status/{id}/{status}', ['uses' => 'ReqController@switch_status', 'as' => 'admin.reqs.switch_status']);
	Route::post('/reqs/load_file/{id}', ['uses' => 'ReqController@load_file', 'as' => 'admin.reqs.load_file']);
	Route::get('/reqs/{id}/first', ['uses' => 'ReqController@first', 'as' => 'admin.reqs.first']);
	Route::get('/reqs/{id}/update_watch', ['uses' => 'ReqController@update_watch', 'as' => 'admin.reqs.update_watch']);
	Route::get('/contacts/offer', ['uses' => 'ContactsController@offer', 'as' => 'admin.contacts.offer']);
    Route::get('/offer/{id}/edit', ['uses' => 'ContactsController@edit', 'as' => 'admin.contacts.offer_edit']);
    Route::put('/offer/{id}', ['uses' => 'ContactsController@update', 'as' => 'admin.contacts.offer_update']);
	Route::resource('reqs', 'ReqController');
	Route::resource('contacts', 'ContactsController');
	Route::resource('questions', 'QuestionController');
	# Admin teams
	Route::get('/teams/{id}/switch_active', ['uses' => 'TeamController@switch_active', 'as' => 'admin.teams.switch_active']);
	Route::get('/teams/{id}/fav', ['uses' => 'TeamController@fav', 'as' => 'admin.teams.fav']);
	Route::post('/teams/reorder', ['uses' => 'TeamController@reorder', 'as' => 'admin.teams.reorder']);
	Route::resource('teams', 'TeamController');
	# Admin events
	Route::get('/sport-events/{id}/switch_active', ['uses' => 'SportEventController@switch_active', 'as' => 'admin.sport-events.switch_active']);
	Route::resource('sport-events', 'SportEventController');
	# Admin catalogs
	Route::resource('catalogs', 'CatalogController');
	Route::resource('catalogs.items', 'CatalogItemController');
	# Admin rules
	Route::resource('rules', 'RuleController');

	Route::resource('paytypes', 'PayTypeController');
	Route::resource('reqtypes', 'ReqTypeController');
	Route::resource('manuals', 'ManualController');

	Route::get('/subscribes', ['uses' => 'SubscribeController@index', 'as' => 'admin.subsribe.index']);
});
Route::group([
	'prefix' => 'api/v1',
	'namespace' => 'Api\V1',
], function () {
	Route::get('/faq/get', ['uses' => 'FaqController@get']);
	Route::get('/req/get', ['uses' => 'ReqController@get']);
	Route::get('/req/get_regions', ['uses' => 'ReqController@get_regions']);
	Route::get('/req/get_areas', ['uses' => 'ReqController@get_areas']);
	Route::get('/req/get_places', ['uses' => 'ReqController@get_places']);
	Route::get('/req/get_type_st', ['uses' => 'ReqController@get_type_st']);
	Route::post('/req/new_request', ['uses' => 'ReqController@new_request']);
	Route::get('/req/get_price', ['uses' => 'ReqController@get_price']);
	Route::get('/req/get_types', ['uses' => 'ReqController@get_types']);
	Route::post('/question/send', ['uses' => 'QuestionController@send']);
	Route::get('/contacts/get', ['uses' => 'ContactsController@get']);
	Route::get('/contacts/offer', ['uses' => 'ContactsController@offer']);
});


# Logout to redirect login page
Route::get('/logout', function () {
	Auth::logout();
	Session::flush();
	return Redirect::to('/auth/login');
})->before('auth.basic');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/', function () {
	View::share('staticversion', Config::get('static.version'));
    $contact = Contacts::where('id', 1)->first();

	return view('welcome', compact('contact'));
});

Route::get('/offer', ['uses' => 'WelcomeController@offer']);
Route::get('/faq', ['uses' => 'WelcomeController@faq']);

Route::get('/admin/', [
	'middleware' => ['auth', 'roles'],
	'roles' => ['administrator', 'manager'],
	'uses' => 'Admin\ReqController@index'
]);
